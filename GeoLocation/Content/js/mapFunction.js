﻿var side_bar_html = "";
var gmarkers = [];
var map = null;
var circle = null;
var geocoder = new google.maps.Geocoder();

function createMarker(latlng, name, html) {
    var contentString = html;
    var marker = new google.maps.Marker({
        position: latlng,
        title: name,
        zIndex: Math.round(latlng.lat() * -100000) << 5
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(contentString);
        infowindow.open(map, marker);
    });

    gmarkers.push(marker);
    side_bar_html += '<a href="javascript:myclick(' + (gmarkers.length - 1) + ')">' + name + '<\/a><br>';
}

function myclick(i) {
    google.maps.event.trigger(gmarkers[i], "click");
}

function initialize() {
    var myOptions = {
        zoom: 5,
        center: new google.maps.LatLng(55.3617609, -3.4433238),
        mapTypeControl: true,
        mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU },
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(document.getElementById("map_canvas"),
                                  myOptions);

    google.maps.event.addListener(map, 'click', function () {
        infowindow.close();
    });

    downloadUrl("/Content/xml/data.xml", function (doc) {
        var xmlDoc = xmlParse(doc);
        var markers = xmlDoc.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
            var lat = parseFloat(markers[i].getAttribute("lat"));
            var lng = parseFloat(markers[i].getAttribute("lng"));
            var point = new google.maps.LatLng(lat, lng);
            var id = markers[i].getAttribute("id");
            var firm = markers[i].getAttribute("firm");
            var email = markers[i].getAttribute("email");
            var phone = markers[i].getAttribute("phone");
            var address = markers[i].getAttribute("address");
            var contact = markers[i].getAttribute("contact");

            var html = "<b>" + firm + "</b><br>" + address;
            var marker = createMarker(point, "<div class='advisor'><div class='leftSection'><div class='firmName'><h4>" +
                firm + "</h4></div><div class='address'><p>" +
                address + "</p></div></div><div class='rightSection'><div class='contact'><span class='contact-name'><span class='label'>Contact: </span>"
                + contact + "</span><br /><span class='contact-email'><span class='label'>Email: </span><a href=mailto:"
                + email + ">"
                + email + "</a></span><br /><span class='phone-number'><span class='label'>Phone: </span>"
                + phone + "</span></div></div></div>", html);

        }
        document.getElementById("side_bar").innerHTML = side_bar_html;
    });
}

function makeSidebar() {
    side_bar_html = "";
    for (var i = 0; i < gmarkers.length; i++) {
        if (map.getBounds().contains(gmarkers[i].getPosition())) {
            side_bar_html += '<div class="result"><a href="javascript:myclick(' + i + ')">' + gmarkers[i].title + '<\/a><br></div>';
        }
    }
    document.getElementById("side_bar").innerHTML = side_bar_html;
}

function codeAddress() {
    var address = document.getElementById('address').value;
    var radius = parseInt(document.getElementById('radius').value, 10) * 1000;
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            side_bar_html = "";
            map.setCenter(results[0].geometry.location);
            var searchCenter = results[0].geometry.location;
            if (circle) circle.setMap(null);
            circle = new google.maps.Circle({
                center: searchCenter,
                radius: radius,
                map: map
            });
            var bounds = new google.maps.LatLngBounds();
            var foundMarkers = 0;
            for (var i = 0; i < gmarkers.length; i++) {
                if (google.maps.geometry.spherical.computeDistanceBetween(gmarkers[i].getPosition(), searchCenter) < radius) {
                    bounds.extend(gmarkers[i].getPosition())
                    gmarkers[i].setMap(map);
                    side_bar_html += '<a href="javascript:myclick(' + i + ')">' + gmarkers[i].title + '<\/a><br>';
                    foundMarkers++;
                } else {
                    gmarkers[i].setMap(null);
                }
            }

            document.getElementById("side_bar").innerHTML = side_bar_html;
            


            if (foundMarkers > 0) {
                map.fitBounds(bounds);
            } else {
                map.fitBounds(circle.getBounds());
            }

            google.maps.event.addListenerOnce(map, 'bounds_changed', makeSidebar);

        } else {
            alert('Unable to find your postcode, check and try again or see all results below.');
        }
    });
}

var infowindow = new google.maps.InfoWindow(
  {
      size: new google.maps.Size(300, 100)
  });